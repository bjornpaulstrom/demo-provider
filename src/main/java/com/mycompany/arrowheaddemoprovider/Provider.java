package com.mycompany.arrowheaddemoprovider;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Björn
 */
public class Provider {

    private static HttpServer server = null;
    private static int port = 30000;
    private static String ipAddress = "";
    private static String serviceUri = "/ourServiceUri";

    public static void main(String[] args) {

        InetAddress inetAddress = null;
        try {
            inetAddress = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(Provider.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (inetAddress == null) {
            return;
        }

        ipAddress = inetAddress.getHostAddress();

        startServer();

        String message = createServiceRegistryObject();

        boolean result = sendSRPOST(message);
        
        if(result == true) {
            System.out.println("Successfully registered provider");
        } else {
            System.out.println("Failed to register provider");
        }
        
    }

    

    private static void startServer() {
        System.out.println("Starting webserver on ip:" + ipAddress + ", port: " + port);
        try {
            server = HttpServer.create(new InetSocketAddress(ipAddress, port), 0);
        } catch (IOException ex) {
            Logger.getLogger(Provider.class.getName()).log(Level.SEVERE, null, ex);
        }

        ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);

        server.createContext(serviceUri, new MyHttpHandler());
        server.setExecutor(threadPoolExecutor);
        server.start();
    }

    private static boolean sendSRPOST(String message) {
        URL obj = null;
        try {
            obj = new URL("http://192.168.54.175:8443/serviceregistry/mgmt");
        } catch (MalformedURLException ex) {
            Logger.getLogger(Provider.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (obj == null) { return false; }

        HttpURLConnection con = null;

        try {
            con = (HttpURLConnection) obj.openConnection();
        } catch (IOException ex) {
            Logger.getLogger(Provider.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (con == null) { return false; }

        con.setRequestProperty("Content-Type", "application/json");

        try {
            con.setRequestMethod("POST");
        } catch (ProtocolException ex) {
            Logger.getLogger(Provider.class.getName()).log(Level.SEVERE, null, ex);
        }

        con.setDoOutput(true);
        OutputStream os = null;
        try {
            os = con.getOutputStream();
        } catch (IOException ex) {
            Logger.getLogger(Provider.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (os == null) { return false; }

        try {
            os.write(message.getBytes());
            os.flush();
            os.close();
        } catch (IOException ex) {
            Logger.getLogger(Provider.class.getName()).log(Level.SEVERE, null, ex);
        }

        int responseCode = 0;
        try {
            responseCode = con.getResponseCode();
        } catch (IOException ex) {
            Logger.getLogger(Provider.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("POST Response Code: " + responseCode);

        if (responseCode == HttpURLConnection.HTTP_CREATED) { //success
            BufferedReader in = null;
            try {
                in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            } catch (IOException ex) {
                Logger.getLogger(Provider.class.getName()).log(Level.SEVERE, null, ex);
            }
            String inputLine;
            StringBuilder response = new StringBuilder();

            if (in == null) {
                return false;
            }

            try {
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(Provider.class.getName()).log(Level.SEVERE, null, ex);
            }

            System.out.println(response.toString());
            return true;
        } else {
            return false;
        }
    }

    private static String createServiceRegistryObject() {
        JSONObject body = new JSONObject();

        body.put("serviceDefinition", "our-service-type");

        JSONObject providerSystem = new JSONObject();
        providerSystem.put("systemName", "our-system-name");
        providerSystem.put("address", ipAddress);
        providerSystem.put("port", port);
        providerSystem.put("authenticationInfo", "");

        body.put("providerSystem", providerSystem);
        body.put("serviceUri", serviceUri);
        body.put("secure", "NOT_SECURE");

        JSONObject metadata = new JSONObject();
        body.put("metadata", metadata);

        JSONArray interfaces = new JSONArray();
        interfaces.put("HTTP-INSECURE-JSON");
        body.put("interfaces", interfaces);
        return body.toString();

    }
}

class MyHttpHandler implements HttpHandler {

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {

        String requestParamValue = null;

        if ("GET".equals(httpExchange.getRequestMethod())) {
            handleGETResponse(httpExchange, requestParamValue);
        } else if ("POST".equals(httpExchange.getRequestMethod())) {
            handlePOSTResponse(httpExchange, requestParamValue);
        }

    }

    private void handleGETResponse(HttpExchange httpExchange, String requestParamValue) throws IOException {
        System.out.println("Got GET request");
        OutputStream outputStream = httpExchange.getResponseBody();
        JSONObject body = new JSONObject();
        body.put("Method", "GET");
        httpExchange.sendResponseHeaders(200, body.toString().length());
        outputStream.write(body.toString().getBytes());
        outputStream.flush();
        outputStream.close();
    }

    private void handlePOSTResponse(HttpExchange httpExchange, String requestParamValue) throws IOException {
        System.out.println("Got POST request");
        OutputStream outputStream = httpExchange.getResponseBody();
        JSONObject body = new JSONObject();
        body.put("Method", "POST");
        httpExchange.sendResponseHeaders(200, body.toString().length());
        outputStream.write(body.toString().getBytes());
        outputStream.flush();
        outputStream.close();
    }
}
